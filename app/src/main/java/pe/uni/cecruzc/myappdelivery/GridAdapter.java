package pe.uni.cecruzc.myappdelivery;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class GridAdapter extends BaseAdapter {
    Context context;
    ArrayList<String> textTitle;
    ArrayList<String> textDescription;
    ArrayList<Integer> image;

    public GridAdapter(Context context, ArrayList<String> text_title, ArrayList<String> text_description, ArrayList<Integer> image) {
        this.context = context;
        this.textTitle = text_title;
        this.textDescription = text_description;
        this.image = image;
    }

    @Override
    public int getCount() {
        return textTitle.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        @SuppressLint("ViewHolder") View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_view_layout, parent, false);

        ImageView imageView = view.findViewById(R.id.image_view_food);
        TextView textTitleView = view.findViewById(R.id.text_view_food_title);
        TextView textDescriptionView = view.findViewById(R.id.text_view_food_description);

        imageView.setImageResource(image.get(position));
        textTitleView.setText(textTitle.get(position));
        textDescriptionView.setText(textDescription.get(position));

        return view;
    }

}
