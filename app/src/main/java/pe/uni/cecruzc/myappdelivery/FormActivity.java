package pe.uni.cecruzc.myappdelivery;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

public class FormActivity extends AppCompatActivity {

    String foodTitle, name, address;
    int quantity = 1;
    boolean isCashSelected, isVisaSelected;

    LinearLayout linearLayout;
    TextView orderTitleTextView;
    Spinner quantitySpinner;
    EditText nameEditText, addressEditText;
    RadioButton cashRadioButton, visaRadioButton;
    Button sendOrderButton;

    SharedPreferences sharedPreferences;

    ArrayAdapter<CharSequence> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        orderTitleTextView = findViewById(R.id.text_view_order_title);
        quantitySpinner = findViewById(R.id.spinner_order_quantity);
        nameEditText = findViewById(R.id.edit_text_order_owner_name);
        addressEditText = findViewById(R.id.edit_text_order_owner_address);
        cashRadioButton = findViewById(R.id.radio_button_order_payment_cash);
        visaRadioButton = findViewById(R.id.radio_button_order_payment_visa);
        sendOrderButton = findViewById(R.id.button_order_send);
        linearLayout = findViewById(R.id.linear_layout);

        foodTitle = getIntent().getStringExtra("foodTitle");

        adapter = ArrayAdapter.createFromResource(this, R.array.numbers, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        quantitySpinner.setAdapter(adapter);

        quantitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                quantity = position + 1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        orderTitleTextView.setText(
                String.format(
                        getResources().getString(R.string.text_view_order_title_text),
                        foodTitle));

        sendOrderButton.setOnClickListener(v -> {
            name = nameEditText.getText().toString();
            address = addressEditText.getText().toString();

            if(cashRadioButton.isChecked()) {
                isCashSelected = true;
                isVisaSelected = false;
            }
            else if(visaRadioButton.isChecked()) {
                isVisaSelected = true;
                isCashSelected = false;
            }

            if(name.equals("") || address.equals("") || (isCashSelected && isVisaSelected)) {
                Snackbar.make(linearLayout, R.string.msg_snack_bar, Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.button_snack_bar_text, v1 -> {

                }).show();
            }
            else {
                String payMethod = "";
                if(isCashSelected)  payMethod = "Efectivo";
                else if(isVisaSelected) payMethod = "Visa";
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(FormActivity.this);
                alertDialog
                        .setTitle(R.string.dialog_title)
                        .setMessage(String.format(
                                getResources().getString(R.string.dialog_text),
                                foodTitle, quantity, name, address, payMethod))
                        .setNegativeButton(R.string.dialog_no, (dialog, which) -> dialog.cancel())
                        .setPositiveButton(R.string.dialog_yes, (dialog, which) -> {
                            moveTaskToBack(true);
                            android.os.Process.killProcess(android.os.Process.myPid());
                            System.exit(1);
                        })
                        .show();
                alertDialog.create();

            }
        });

        retrieveData();
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }

    private void saveData() {
        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);
        name = nameEditText.getText().toString();
        address = addressEditText.getText().toString();
        if(cashRadioButton.isChecked()) {
            isCashSelected = true;
            isVisaSelected = false;
        }
        else if(visaRadioButton.isChecked()) {
            isVisaSelected = true;
            isCashSelected = false;
        }

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("key name", name);
        editor.putString("key address", address);
        editor.putInt("key quantity", quantity);
        editor.putBoolean("key cash", isCashSelected);
        editor.putBoolean("key visa", isVisaSelected);
        editor.apply();

        Toast.makeText(getApplicationContext(), "Tus datos están guardados", Toast.LENGTH_LONG).show();
    }

    private void retrieveData() {
        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);
        name = sharedPreferences.getString("key name", null);
        address = sharedPreferences.getString("key address", null);
        quantity = sharedPreferences.getInt("key quantity", 1);
        isCashSelected = sharedPreferences.getBoolean("key cash", false);
        isVisaSelected = sharedPreferences.getBoolean("key visa", false);

        nameEditText.setText(name);
        addressEditText.setText(address);
        cashRadioButton.setChecked(isCashSelected);
        visaRadioButton.setChecked(isVisaSelected);
    }


}