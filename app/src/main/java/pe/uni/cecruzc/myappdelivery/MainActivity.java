package pe.uni.cecruzc.myappdelivery;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    GridView gridView;
    ArrayList<String> textTitle = new ArrayList<>();
    ArrayList<String> textDescription = new ArrayList<>();
    ArrayList<Integer> image = new ArrayList<>();
    SharedPreferences sharedPreferences;
    String foodTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gridView = findViewById(R.id.grid_view);
        fillArray();

        GridAdapter gridAdapter = new GridAdapter(this, textTitle, textDescription, image);
        gridView.setAdapter(gridAdapter);

        gridView.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(MainActivity.this, FormActivity.class);
            foodTitle = textTitle.get(position);
            intent.putExtra("foodTitle", foodTitle);
            saveData();
            startActivity(intent);
            // finish();
        });

        retrieveData();
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }

    private void fillArray() {
        textTitle.add("Aguadito");
        textTitle.add("Aji de Gallina");
        textTitle.add("Arroz con Pollo");
        textTitle.add("Causa");
        textTitle.add("Ceviche");
        textTitle.add("Lomo Saltado");
        textTitle.add("Pachamanca");
        textTitle.add("Pollo a la brasa");

        textDescription.add("Descripción 1");
        textDescription.add("Descripción 2");
        textDescription.add("Descripción 3");
        textDescription.add("Descripción 4");
        textDescription.add("Descripción 5");
        textDescription.add("Descripción 6");
        textDescription.add("Descripción 7");
        textDescription.add("Descripción 8");

        image.add(R.drawable.aguadito);
        image.add(R.drawable.aji_gallina);
        image.add(R.drawable.arroz_pollo);
        image.add(R.drawable.causa);
        image.add(R.drawable.ceviche);
        image.add(R.drawable.lomo_saltado);
        image.add(R.drawable.pachamanca);
        image.add(R.drawable.pollo_brasa);
    }

    private void saveData() {
        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("key food", foodTitle);
        editor.apply();
    }

    private void retrieveData() {
        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);
        foodTitle = sharedPreferences.getString("key food", null);

        if(foodTitle != null) {
            Intent intent = new Intent(MainActivity.this, FormActivity.class);
            intent.putExtra("foodTitle", foodTitle);
            startActivity(intent);
        }
    }
}